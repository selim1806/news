package airweb.fr.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import airweb.fr.news.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import airweb.fr.news.activity.DetailNewsActivity;
import airweb.fr.news.model.ListRetroNews;
import airweb.fr.news.model.RetroNews;

public class CustomNewsAdapter extends RecyclerView.Adapter<CustomNewsAdapter.CustomViewHolder> {

    private ListRetroNews dataList;
    private Context context;

    public CustomNewsAdapter(Context context, ListRetroNews dataList){
        this.context = context;
        this.dataList = dataList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        TextView txtTitle;
        TextView txtContent;
        private ImageView coverImage;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            txtTitle = mView.findViewById(R.id.title);
            txtContent = mView.findViewById(R.id.content);
            coverImage = mView.findViewById(R.id.coverImage);
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_row_news, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.txtTitle.setText(dataList.getNews().get(position).getTitle());
        holder.txtContent.setText(dataList.getNews().get(position).getContent());

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(dataList.getNews().get(position).getPicture())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.coverImage);

        // get the object row clicked and passed it the the DetailNewsActivity
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, DetailNewsActivity.class);
                intent.putExtra("RetroNewsObject", (Serializable) dataList.getNews().get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.getNews().size();
    }
}
