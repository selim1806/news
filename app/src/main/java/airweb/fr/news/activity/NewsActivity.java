package airweb.fr.news.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import airweb.fr.news.R;
import airweb.fr.news.adapter.CustomNewsAdapter;
import airweb.fr.news.model.ListRetroNews;
import airweb.fr.news.network.GetDataService;
import airweb.fr.news.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends AppCompatActivity {

    private CustomNewsAdapter adapter;
    private RecyclerView recyclerView;
    ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        progressDoalog = new ProgressDialog(NewsActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<ListRetroNews> call = service.getAllNews();
        call.enqueue(new Callback<ListRetroNews>() {
            @Override
            public void onResponse(Call<ListRetroNews> call, Response<ListRetroNews> response) {
                progressDoalog.dismiss();
                generateDataList(response.body());


            }

            @Override
            public void onFailure(Call<ListRetroNews> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(NewsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                String message = t.getMessage();
                Log.d("failure", message);
            }
        });
    }

    /*Method to generate List of data using RecyclerView with custom adapter*/
    private void generateDataList(ListRetroNews news) {
        recyclerView = findViewById(R.id.customRecyclerView);
        adapter = new CustomNewsAdapter(this,news);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NewsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }
}
