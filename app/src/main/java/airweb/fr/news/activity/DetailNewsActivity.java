package airweb.fr.news.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import airweb.fr.news.R;
import airweb.fr.news.model.RetroNews;

public class DetailNewsActivity extends AppCompatActivity {

    TextView txtTitle;
    TextView txtContent;
    ImageView coverImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);

        // get the object passed from intent
        Intent intent = getIntent();
        RetroNews retroNews = (RetroNews) getIntent().getSerializableExtra("RetroNewsObject");
        Log.d("OK2",""+retroNews.getTitle());

        txtTitle = (TextView)findViewById(R.id.title);
        txtContent = (TextView)findViewById(R.id.content);
        coverImage = (ImageView)findViewById(R.id.coverImage);

        // init View
        txtTitle.setText(retroNews.getTitle());
        txtContent.setText(retroNews.getContent());

        Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
        builder.downloader(new OkHttp3Downloader(getApplicationContext()));
        builder.build().load(retroNews.getPicture())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(coverImage);
    }


}
