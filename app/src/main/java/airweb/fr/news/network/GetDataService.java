package airweb.fr.news.network;

import java.util.List;

import airweb.fr.news.model.ListRetroNews;
import airweb.fr.news.model.RetroNews;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("/psg/psg.json?")
    Call<ListRetroNews> getAllNews();
}
