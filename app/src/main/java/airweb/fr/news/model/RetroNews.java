package airweb.fr.news.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RetroNews implements Serializable {

    @SerializedName("nid")
    private int nid;
    @SerializedName("type")
    private String type;
    @SerializedName("date")
    private String date;
    @SerializedName("title")
    private String title;
    @SerializedName("picture")
    private String picture;
    @SerializedName("content")
    private String content;

    public RetroNews(int nid, String type, String date, String title, String picture, String content) {
        this.nid = nid;
        this.type = type;
        this.date = date;
        this.title = title;
        this.picture = picture;
        this.content = content;
    }

    // Getter
    public int getNid() {
        return nid;
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getPicture() {
        return picture;
    }

    public String getContent() {
        return content;
    }

    // Setter

    public void setNid(int nid) {
        this.nid = nid;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
