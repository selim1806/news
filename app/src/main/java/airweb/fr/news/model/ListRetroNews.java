package airweb.fr.news.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListRetroNews implements Serializable {

    @SerializedName("news")
    private List<RetroNews> news;

    public ListRetroNews(List<RetroNews> news) {
        this.news = news;
    }

    public List<RetroNews> getNews() {
        return news;
    }

    public void setNews(List<RetroNews> news) {
        this.news = news;
    }
}
